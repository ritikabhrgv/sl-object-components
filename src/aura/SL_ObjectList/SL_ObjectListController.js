({
    doInit : function(component, event, helper) {
        
        //Helper method to load all the records for the corresponding SObject
        helper.getSObjectRecords(component);
        component.set("v.globalTimeout", null); 
    },
    
    showSpinner : function (component, event, helper) {
        component.set("v.spinnerLoad", "true");    
    },
    
    hideSpinner : function (component, event, helper) {
        component.set("v.spinnerLoad", "false");    
    },
    
    showMore : function(component, event, helper) {
        
        //Helper method to load More records
        helper.loadMoreRecords(component);
    },
    
    searchRecords: function(component, event, helper) {
        var strPreviousSearch = component.get('v.strSearchString');
        
        if(component.get("v.globalTimeout") != null) clearTimeout(component.get("v.globalTimeout"));  
        component.set("v.globalTimeout", window.setTimeout($A.getCallback(function() {SearchFunc()}),1000));  
        
        function SearchFunc() {  
            component.set("v.globalTimeout", null);  
            
            var strSearchString = component.find("searchInput").get("v.value").trim();
            var strWithoutSpecChar = strSearchString.replace(/[^\w\s]/gi, '');
            if(strPreviousSearch != strSearchString && (strSearchString == '' || strWithoutSpecChar.length >= 2)) {
                component.set('v.strSearchString', strSearchString);
                //Helper method to Search the records for the corresponding Searched Term
                helper.getSObjectRecords(component);
            }
        }
    },
    
    onPicklistChange: function(component, event, helper) {
        
        var picklistCmp = component.find("picklistOptions");
        component.set("v.SelectedPicklitsValue", picklistCmp.get("v.value"));
        helper.getSObjectRecords(component);
    }
})