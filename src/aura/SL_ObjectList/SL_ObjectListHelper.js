({
    getSObjectRecords : function(cmp) {
        
        var sObjectName = cmp.get("v.SObjectName"),
            fieldSetName = cmp.get("v.FieldsetName"),
            maxRecords = cmp.get("v.MaxRecords"),
            recordsOnLoad = cmp.get("v.RecordsOnLoad"),
            filterCriteria = cmp.get("v.FilterCriteria"),
            picklistFilter = cmp.get("v.PicklistFilter"),
            SelectedPicklitsValue = cmp.get("v.SelectedPicklitsValue"),
            topicId = cmp.get("v.TopicId"),
            searchString = cmp.get('v.strSearchString');
        
        if(! maxRecords)
            maxRecords = 100;
        
        var action = cmp.get("c.fetchSObjectRecords");
        action.setParams({"strSObjectName" : sObjectName,
                          "strFieldsetName" : fieldSetName,
                          "intMaxRecordsToQuery" : maxRecords,
                          "strFilterCriteria" : filterCriteria,
                          "strPicklistFieldAPIName" : picklistFilter,
                          "strSelectedPicklistValue" : SelectedPicklitsValue,
                          "strFilterTopicId" : topicId,
                          "strSearchString" : searchString});
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(cmp.isValid() && state === "SUCCESS") {
                
                var ret = response.getReturnValue();
                if(!ret) return;
                
                cmp.set("v.lstSObjectRecords", ret.lstSObjectRecords);
                cmp.set("v.lstFieldsetResults", ret.fieldSetResults);
                
                if(ret.lstSObjectRecords.length > 0 && ret.fieldSetResults.length > 0) {
                    
                    var totalRecords = ret.lstSObjectRecords.length,
                        lstRecordsDisplayed = [];
                    
                    for(var i = 0; i < recordsOnLoad; i++)
                        if(ret.lstSObjectRecords[i])
                            lstRecordsDisplayed.push(ret.lstSObjectRecords[i]);
                    
                    cmp.set("v.totalRecords", totalRecords);
                    cmp.set("v.lstRecordsDisplayed", lstRecordsDisplayed);
                    cmp.set("v.intRecordsDisplayed", lstRecordsDisplayed.length);
                    
                    if(totalRecords > recordsOnLoad)
                        cmp.set("v.hasMoreRecords", true);
                    else
                        cmp.set("v.hasMoreRecords", false);
                    
                    var optns = [];
                    for (var key in ret.picklistOptions) {
                        if (ret.picklistOptions.hasOwnProperty(key)) {
                            if(SelectedPicklitsValue === key) {
                                var optn = { label: key, value: ret.picklistOptions[key], selected: "true"};
                                optns.push(optn);
                            }
                            else {
                                var optn = { label: key, value: ret.picklistOptions[key]};
                                optns.push(optn);
                            }
                        }
                    }
                    cmp.set("v.intPicklistOptions", optns.length);
                    cmp.find("picklistOptions").set("v.options", optns);
                }
                else {
                    cmp.set("v.totalRecords", 0);
                    cmp.set("v.lstRecordsDisplayed", undefined);
                    cmp.set("v.intRecordsDisplayed", 0);
                    cmp.set("v.hasMoreRecords", false);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    
    //Method to load more records on click of View More button
    loadMoreRecords : function(cmp) {
        
        var intMoreRecordsToAdd = cmp.get("v.RecordsOnLoad"),
            intRecordsDisplayed = cmp.get("v.intRecordsDisplayed"),
            lstSObjectRecords = cmp.get("v.lstSObjectRecords"),
            lstRecordsDisplayed = cmp.get("v.lstRecordsDisplayed"),
            totalRecords = cmp.get("v.totalRecords"),
            newTotalRecords = intRecordsDisplayed + intMoreRecordsToAdd;
        
        for(var i = intRecordsDisplayed; i < newTotalRecords; i++ ) {
            if(lstSObjectRecords[i])
                lstRecordsDisplayed.push(lstSObjectRecords[i]);
        }
        
        if(newTotalRecords >= totalRecords)
            cmp.set("v.hasMoreRecords", false);
        
        cmp.set("v.lstRecordsDisplayed", JSON.parse(JSON.stringify(lstRecordsDisplayed)));
        cmp.set("v.intRecordsDisplayed", newTotalRecords);
    }
})