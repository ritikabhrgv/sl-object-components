({
    doInit : function(component) {
        
        //Display the first field in the Fieldset as the header for the record
        var record = component.get("v.SObjectRecord");
        var FieldsetResults = component.get("v.FieldsetResults");
        if(FieldsetResults) {
            
            var fieldName = FieldsetResults[0].fieldPath;
            
            /* Function that recompose the reference fields.
             * eg: Oppty.Account.Name recomposes to Record['Oppty']['Account']['Name']
             */
            function getRecordValue(value,fieldName) {
                var lstFields = fieldName.split('.');
                var recordValue = value[lstFields[0]];
                if(lstFields[1] && recordValue){
                    lstFields.splice(0,1);
                    var newFieldName = lstFields.join('.');
                    return getRecordValue(recordValue,newFieldName);
                }
                return recordValue;
            }
            
            component.set("v.firstFieldValue", getRecordValue(record, fieldName));
            component.set("v.firstField", fieldName);
        }
        
        var parentElement = component.find('cardHeader');
        if(component.get('v.IsExpanded'))
            $A.util.addClass(parentElement, '_open');
    },
    
    openClose : function(component) {
        
        var parentElement = component.find('cardHeader');
        $A.util.toggleClass(parentElement, '_open');
    },
    
    navigateToRecord : function (component, event, helper) {
        
        var isObjectDetailComponent = component.get("v.IsObjectDetailComponent");
        var record = component.get("v.SObjectRecord");
        
        //If On-click is fired from Object Detail component, it will be redirected to Record Detail page
        //Else if fired from Object List widget, a event to Object Detail is fired.
        if(isObjectDetailComponent) {
            
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": record.Id,
                "slideDevName": "detail"
            });
            navEvt.fire();
        }
        else {
            var isDisableLinks = component.get("v.DisableLinks");
            if(! isDisableLinks) {	//Event for Object Detail fired only when Disabled Links attribute is set false.
                
                var showDetailEvent = $A.get("e.c:ShowObjectDetailEvent");
                showDetailEvent.setParams({ "recordId" : record.Id, "strSObjectName" : component.get("v.SObjectName") });
                showDetailEvent.fire();
            }
        }
        
        event.stopImmediatePropagation();
    }
})