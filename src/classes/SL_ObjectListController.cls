public with sharing class SL_ObjectListController {
    
    //Wrapper class for the complete results returned back to the component controller
    public class ResultsWrapper {
        
        @AuraEnabled
        public List<SObject> lstSObjectRecords { get;set; }
        @AuraEnabled
        public List<FieldSetMember> fieldSetResults { get;set; }
        @AuraEnabled
        public map<String, String> picklistOptions	{get;set;}
        
        //Constructor
        public ResultsWrapper(List<SObject> lstSObjectRecords, List<FieldSetMember> fieldSetResults, map<String, String> picklistOptions) {
            this.lstSObjectRecords = lstSObjectRecords;
            this.fieldSetResults = fieldSetResults;
            this.picklistOptions = picklistOptions;
        }
    }
    
    //Inner class for FieldSetMember
    public class FieldSetMember {
        
        @AuraEnabled
        public Boolean DBRequired { get;set; }
        @AuraEnabled
        public String fieldPath { get;set; }
        @AuraEnabled
        public String label { get;set; }
        @AuraEnabled
        public Boolean required { get;set; }
        @AuraEnabled
        public String type { get; set; }
        
        public FieldSetMember(Schema.FieldSetMember fsm) {
            this.DBRequired = fsm.DBRequired;
            this.fieldPath = fsm.fieldPath;
            this.required = fsm.required;
            this.type = '' + fsm.getType();
            
            String strHierarchyLabel = '';
            list<String> lstHierarchy = fsm.fieldPath.split('\\.');
            
            if(lstHierarchy.size() > 1) {
                lstHierarchy.remove(lstHierarchy.size() - 1);
                for(String strObjName : lstHierarchy) {
                    if(Schema.getGlobalDescribe().containsKey(strObjName)) {
                        Schema.DescribeSObjectResult describedSObject = Schema.getGlobalDescribe().get(strObjName).getDescribe();
                        strHierarchyLabel += describedSObject.getLabel() + '>';
                    }
                    else
                        strHierarchyLabel += strObjName + '>';
                }
            }
            
            this.label = strHierarchyLabel + fsm.label;
        }
    }
    
    
    /* 
    * Name: fetchSObjectRecords
    * Parameters: String strSObjectName, 
    * 			  String strFieldsetName, 
    * 			  Integer intMaxRecordsToQuery, 
    * 			  String strFilterCriteria, 
    * 			  String strPicklistFieldAPIName, 
    * 			  String strSelectedPicklistValue, 
    * 			  String strFilterTopicId, 
    * 			  String strSearchString
    * Description: Returns all the reqired information that is Queried and required for the Object List widget
    */
	@AuraEnabled
    public static ResultsWrapper fetchSObjectRecords(String strSObjectName, String strFieldsetName, Integer intMaxRecordsToQuery, String strFilterCriteria, String strPicklistFieldAPIName, String strSelectedPicklistValue, String strFilterTopicId, String strSearchString) {
        
        Schema.DescribeSObjectResult describedSObject = Schema.getGlobalDescribe().get(strSObjectName).getDescribe();
        
        set<String> setUnaccessibleFields = new set<String>();	//Stores the Set of Unaccessable fields for the Object
        //Checking if all the fields in the Object are accessible (Security Review check)
        Map<String, Schema.SObjectField> fieldMap = describedSObject.fields.getMap();
        for (String fieldName : fieldMap.keySet()) {
            Schema.DescribeFieldResult descField = fieldMap.get(fieldName).getDescribe();
            if(!descField.isaccessible())
                setUnaccessibleFields.add(descField.getName());
        }
        
        String strQuery = 'SELECT Id';
        List<FieldSetMember> fieldSetResults = new List<FieldSetMember>();
        if(describedSObject.FieldSets.getMap().containsKey(strFieldsetName)) {
            List<Schema.FieldSetMember> fieldSetMemberList =  describedSObject.FieldSets.getMap().get(strFieldsetName).getFields();
            for(Schema.FieldSetMember objField : fieldSetMemberList) {
                if(! setUnaccessibleFields.contains(objField.getFieldPath()) && objField.getFieldPath() != 'ID')
                    strQuery += ', '+ objField.getFieldPath();
                fieldSetResults.add(new FieldSetMember(objField));
            }
        }
        
        String strFinalQuery = strQuery + ' From ' + strSObjectName;
        String strOrderBy = '';
        
        //Adding the filter criteria(if any)
        if(String.isNotBlank(strFilterCriteria)) {
            strFilterCriteria = strFilterCriteria.trim();
            
            if(strFilterCriteria.containsIgnoreCase('ORDER BY')) {
                Integer index = strFilterCriteria.indexOfIgnoreCase('order by');
                strOrderBy = ' ORDER BY ' + strFilterCriteria.substring(index+8, strFilterCriteria.length());
                strFilterCriteria = strFilterCriteria.substring(0, index);
            }
            
            String userId = (String) userinfo.getuserid();
            list<User> lstUser = [select Id, ContactId 
                                  From User
                                  Where Id =: userId];
            
            //Supporting CurrentUser in filtercriteria
            while(strFilterCriteria.containsIgnoreCase('currentuser')) {
                Integer index = strFilterCriteria.indexOfIgnoreCase('currentuser');
                String temp = strFilterCriteria.substring(index, index+11);
                strFilterCriteria = strFilterCriteria.replaceFirst(temp, '\''+userId+'\'');
            }
            
            //Supporting CurrentUserContact in filtercriteria
            while(strFilterCriteria.containsIgnoreCase('current_user_contact')) {
                String strContactId = '';
                if(! lstUser.isEmpty() && lstUser[0].ContactId != null) {
                    strContactId = lstUser[0].ContactId;
                    Integer index = strFilterCriteria.indexOfIgnoreCase('current_user_contact');
                    String temp = strFilterCriteria.substring(index, index+20);
                    strFilterCriteria = strFilterCriteria.replaceFirst(temp, '\''+strContactId+'\'');
                }
                else
                    strFilterCriteria = strFilterCriteria.substringAfter('current_user_contact');
            }
            
            if(String.isNotBlank(strFilterCriteria.trim())) {
                strFilterCriteria = strFilterCriteria.trim();
                
                if(strFilterCriteria.startsWithIgnoreCase('AND')) {
                    Integer index = strFilterCriteria.indexOfIgnoreCase('AND');
                    String temp = strFilterCriteria.substring(index, index+4);
                    strFilterCriteria = strFilterCriteria.replaceFirst(temp, '');
                }
                else if(strFilterCriteria.startsWithIgnoreCase('OR')) {
                    Integer index = strFilterCriteria.indexOfIgnoreCase('OR');
                    String temp = strFilterCriteria.substring(index, index+3);
                    strFilterCriteria = strFilterCriteria.replaceFirst(temp, '');
                }
            }
            
            if(String.isNotBlank(strFilterCriteria.trim()))
                strFinalQuery += ' WHERE ' + strFilterCriteria.trim();
        }
        else if(!fieldSetResults.isEmpty())
            strOrderBy = ' ORDER BY ' + fieldSetResults[0].fieldPath;
        
        //Picklist Filtering
        map<String, String> mapPicklistOptions = new map<String, String>();
        if(String.isNotBlank(strPicklistFieldAPIName) && fieldMap.containsKey(strPicklistFieldAPIName) && !setUnaccessibleFields.contains(strPicklistFieldAPIName)) {
            mapPicklistOptions.put('--None--', '');
            Schema.DescribeFieldResult fieldResult = fieldMap.get(strPicklistFieldAPIName).getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            for( Schema.PicklistEntry f : ple)
                mapPicklistOptions.put(f.getLabel(), f.getValue());
            
            if(String.isNotBlank(strSelectedPicklistValue)) {
                if(strFinalQuery.contains(' WHERE '))
                    strFinalQuery += (' AND ' + strPicklistFieldAPIName + ' = \'' + strSelectedPicklistValue + '\'');
                else
                    strFinalQuery += (' WHERE ' + strPicklistFieldAPIName + ' = \'' + strSelectedPicklistValue + '\'');
            }
        }
        
        //Broadcasted Chatter Topic Id filtering
        List<Id> topicEntitiesIds = new list<Id>();
        if(String.isNotBlank(strFilterTopicId)) {
            for(TopicAssignment obj : [SELECT Id, EntityId FROM TopicAssignment WHERE TopicId =: strFilterTopicId AND EntityKeyPrefix =: describedSObject.getKeyPrefix()])
                topicEntitiesIds.add(obj.EntityId);
            
            if(! topicEntitiesIds.isEmpty())
                if(strFinalQuery.contains(' WHERE '))
                	strFinalQuery += ' AND Id IN : topicEntitiesIds';
            	else
                    strFinalQuery += ' WHERE Id IN : topicEntitiesIds';
        }
        
        //Filtering the records via searched String
        set<Id> setSearchFilteredIds = new set<Id>();
        if(String.isNotBlank(strSearchString)) {
            setSearchFilteredIds = searchFilterRecords(strSearchString, strSObjectName);
            if(strFinalQuery.contains(' WHERE '))
                strFinalQuery += ' AND Id IN : setSearchFilteredIds';
            else
                strFinalQuery += ' WHERE Id IN : setSearchFilteredIds';
        }
        
        //Additional Where clause for the Knowledge Article Records
        if(strSObjectName.endsWith('kav')) {
            if(strFinalQuery.contains(' WHERE '))
                strFinalQuery += ' AND PublishStatus =\'online\' and Language = \'en_US\' ';
            else
                strFinalQuery += ' WHERE PublishStatus =\'online\' and Language = \'en_US\' ';
        }
        
        strFinalQuery += strOrderBy + ' LIMIT ' + intMaxRecordsToQuery;
        system.debug('final Query::'+strFinalQuery);
        List<SObject> lstSObjectRecords = Database.query(strFinalQuery);
        system.debug('lstSObjectRecords::'+lstSObjectRecords);
        
        return new ResultsWrapper(lstSObjectRecords, fieldSetResults, mapPicklistOptions);
    }
    
    /* 
    * Name: searchFilterRecords
    * Parameters: String strSearchTerm, String sObjectName
    * Description: Filter all the records that matches the search terms and returns list of such records
    */
    public static set<Id> searchFilterRecords(String strSearchTerm, String sObjectName) {
        
        final set<String> setValidLikeOperatorDataTypes = new Set<String> {'URL', 'PHONE', 'EMAIL', 'PICKLIST', 'STRING'};
        set<Id> setsObjectFilteredIds = new set<Id>();
        String strSearchQuery = '';
        
        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap();
        
        for(Schema.SObjectField objField : fieldMap.Values())
        {
            Schema.describefieldresult field = objField.getDescribe();
            
            if(String.ValueOf(field.getType()) == 'MULTIPICKLIST' && field.isFilterable())
                strSearchQuery += (strSearchQuery == '' ? field.getName() + ' Includes (\'' +String.escapeSingleQuotes(strSearchTerm)+ '\')' : ' OR ' +field.getName() + ' Includes (\'' +String.escapeSingleQuotes(strSearchTerm)+ '\')');
            else if(setValidLikeOperatorDataTypes.contains(String.ValueOf(field.getType())) && field.getName() != 'Id' && String.ValueOf(field.getType()) != 'MULTIPICKLIST' && field.isFilterable())
                strSearchQuery += (strSearchQuery == '' ? field.getName() + ' LIKE \'%' + String.escapeSingleQuotes(strSearchTerm) + '%\'' : ' OR ' + field.getName() + ' LIKE \'%' + String.escapeSingleQuotes(strSearchTerm) + '%\'');
        }
        

        String stringToQuery = 'Select Id From ' + String.escapeSingleQuotes(sObjectName);
        
        if(String.isNotBlank(strSearchQuery) && !sObjectName.contains('__x')) {
            
            strSearchQuery = ' Where ' + strSearchQuery;
            list<sObject> lstSearchResultsAfterSOQL = database.query(stringToQuery + strSearchQuery);
            for(sObject objSobject : lstSearchResultsAfterSOQL)
                setsObjectFilteredIds.add(objSobject.Id);
        }
        
        //Performing and SOSL for searching in all fields that are not covered in SOQL
        strSearchTerm = '\''+ strSearchTerm + '*\'';
        String strSosl = 'Find ' + strSearchTerm +  ' in ALL FIELDS '+ 'RETURNING ' + String.escapeSingleQuotes(sObjectName);
        
        List<list<sObject>> lstSearchResultsAfterSOSL = new list<list<sObject>>();
        lstSearchResultsAfterSOSL = search.query(strSosl);
        
        for(sObject objSobject : lstSearchResultsAfterSOSL[0]) {
            setsObjectFilteredIds.add(objSobject.Id);
        }
        
        return setsObjectFilteredIds;
    }
    
}