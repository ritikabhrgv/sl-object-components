/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class SL_Test_ObjectComponentsController {
    
    // Test code for SL_ObjectListController
    static testMethod void testObjectList() {
        
        Opportunity objOppty = new Opportunity(Name = 'Test Oppty', StageName = 'Closed Won', Amount = 50000, CloseDate = Date.today());
        
        insert objOppty;
        
        SL_ObjectListController.ResultsWrapper objResults =  SL_ObjectListController.fetchSObjectRecords('Opportunity',
                                                                                                         'OpptyFieldset',
                                                                                                         20, 
                                                                                                         'ContactID = current_user_contact AND OwnerId = CurrentUser AND StageName = \'Closed Won\'',
                                                                                                         'StageName',
                                                                                                         'Closed Won',
                                                                                                         '',
                                                                                                         'Test' );
        system.assertEquals(objResults.lstSObjectRecords.size(), 1);
        
        Topic objTopic = new Topic(Name='Test-Topic');
        insert objTopic;
        TopicAssignment objTopicAssignment = new TopicAssignment(TopicId = objTopic.Id, EntityId = objOppty.Id);
        insert objTopicAssignment;
        
        SL_ObjectListController.fetchSObjectRecords('Opportunity',
                                                    'OpptyFieldset',
                                                    20, 
                                                    'ORDER BY Name',
                                                    '',
                                                    '',
                                                    objTopic.Id,
                                                    '' );
        system.assertEquals(objResults.lstSObjectRecords.size(), 1);
    }
    
    // Test code for SL_ObjectDetailController
    static testMethod void testObjectDetail() {
        
        Opportunity objOppty = new Opportunity(Name = 'Test Oppty', StageName = 'Closed Won', Amount = 50000, CloseDate = Date.today());
        
        insert objOppty;
        
        String sObjectId = objOppty.Id;
        SL_ObjectDetailController.ObjectDetail_InnerClass objResults = SL_ObjectDetailController.getSObjectDetailRecord('Opportunity', 
                                                                                                                        sObjectId, 
                                                                                                                        'OpptyFieldset', 
                                                                                                                        'ContactID = current_user_contact AND OwnerId = CurrentUser AND StageName = \'Closed Won\' ORDER BY Name', 
                                                                                                                        'HIDDEN', 
                                                                                                                        true, 
                                                                                                                        true, 
                                                                                                                        2);
        system.assertEquals(objResults.sObjectDetailRecord.size(), 1);
    }
}